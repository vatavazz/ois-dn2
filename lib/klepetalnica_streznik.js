var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesla = {
  Skedenj : false
};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    preglejZasebno(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    socket.on('kanali', function() {
      uporabniki(socket);
    });
  });
};

function uporabniki(socket) {
  var kan = trenutniKanal[socket.id];
  var vsiUp= [];
  var uporabnikiNaKanalu = io.sockets.clients(kan);
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    vsiUp.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
  }
  socket.emit('uporabniki', vsiUp);
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, geslo) {
  socket.join(kanal);
  if (!(kanal in gesla)) {
    gesla[kanal] = geslo;
  }
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  
  uporabniki(socket);
}

function preglejZasebno(socket) {
  socket.on('posljiZasebno', function(posilja, vzdevek, spor) {;
    if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
      socket.emit('prejmiZasebnoSporocilo', {
        uspesno: false,
        pos: posilja,
        prej: vzdevek,
        sporocilo: 'Sporočilo '+spor+' uporabniku z vzdevkom '+vzdevek+' ni bilo mogoče posredovati.'
      });
    } else {
      //socket.broadcast.to
      var allSock = io.sockets.clients();
      var prejSocket = socket;
      for (var s in allSock) {
        if (vzdevkiGledeNaSocket[allSock[s].id] == vzdevek) {
          prejSocket = allSock[s];
        }
      }
      prejSocket.emit('prejmiZasebnoSporocilo', {
        uspesno: true,
        pos: posilja,
        prej: vzdevek,
        sporocilo: spor
      });
      socket.emit('prejmiZasebnoSporocilo', {
        uspesno: true,
        pos: posilja,
        prej: vzdevek,
        sporocilo: spor
      });
    }
  });
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        uporabniki(socket);
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
  });
  
  socket.on('pridruzitevZahtevaGeslo', function(kanal) {
    var ime = kanal.novKanal;
    console.log(kanal.geslo + '|' + gesla[ime]);
    
    if (ime in gesla) {
      if (gesla[ime] !== false){
          if (gesla[ime] == kanal.geslo) {
            socket.leave(trenutniKanal[socket.id]);
            pridruzitevKanalu(socket, kanal.novKanal);
          }
          else {
            socket.emit('kanaliGeslo', 'Pridružitev v kanal '+ime+' ni bilo uspešno, ker je geslo napačno!');
          }
      } else {
        socket.emit('kanaliGeslo', 'Izbrani kanal '+kanal.novKanal+' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev '+kanal.novKanal+' ali zahtevajte kreiranje kanala z drugim imenom.')
      }
    } else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
    }
  });
  uporabniki(socket);
}


function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
  uporabniki(socket);
}