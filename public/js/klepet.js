var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: false
  });
};

Klepet.prototype.spremeniKanalGeslo = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahtevaGeslo', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz, mojVzd) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      
      //preglej ce je ukaz brez gesla
      if (kanal.indexOf('\"') == -1) {
        //normalno spremeni kanal
        this.spremeniKanal(kanal);
      }
      
      else {
        var vsiUkazi = kanal.split('\"');
        if (vsiUkazi.length != 5) {
          sporocilo = 'Neznan ukaz.';
          break;
        }
        else {
          this.spremeniKanalGeslo(vsiUkazi[1], vsiUkazi[3]);
        }
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var vse = besede.join(' ');
      if (vse.split('\"').length != 5) {
        sporocilo = 'Neznan ukaz.';
        break;
      } else {
        if (vse.split('\"')[1] == mojVzd) {
          sporocilo = 'Sporočilo '+vse.split('\"')[3]+' uporabniku z vzdevkom '+mojVzd+' ni bilo mogoče posredovati.';
          break;
        }
        else {
          this.socket.emit('posljiZasebno', mojVzd, vse.split('\"')[1], vse.split('\"')[3]);
        }
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};