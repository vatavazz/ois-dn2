var words = [];
var mojVzd;

function censor(sporocilo){
  for (var w in words) {
    var filter = words[w].replace(new RegExp('.', 'g'),"*");
    var src = words[w];
    sporocilo = sporocilo.replace(new RegExp("\\b"+ src+ "\\b","gi"), filter);
  }
  return sporocilo;
}




function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo, mojVzd);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    sporocilo = checkBracket(sporocilo);
    sporocilo = censor(sporocilo);
    sporocilo = smilies(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function checkBracket(sporocilo) {
  sporocilo = sporocilo.replace(/>/g, "&gt");
  sporocilo = sporocilo.replace(/</g, "&lt");
  return sporocilo;
}

function smilies(sporocilo) {
  var smile = {
      wink  : ";)",
      smiley  : ":)",
      like : "(y)",
      kiss  : ":*",
      sad  : ":("
    }
    var s;
    for (s in smile) {
      var src = smile[s];
      sporocilo = sporocilo.split(src).join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/" + s +".png' />");
    }
    return sporocilo;
}

var socket = io.connect();
var kan;
var vzd;


$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  $.get('../swearWords.txt', function( data ) {
    words = data.split("\n");
  }).fail(function(jqXHR, textStatus, errorThrown) {alert(errorThrown);});

  socket.on('prejmiZasebnoSporocilo', function(spor) {
    var sporocilo = spor.sporocilo;
    sporocilo = checkBracket(sporocilo);
    sporocilo = censor(sporocilo);
    sporocilo = smilies(sporocilo);
    var novElement;
    if (spor.uspesno) {
      if (spor.prej == mojVzd) {
        novElement = $('<div style="font-weight: bold"></div>').html(spor.pos + ' (zasebno): ' +sporocilo);
        $('#sporocila').append(novElement);
      }
      if (spor.pos == mojVzd) {
        novElement = $('<div style="font-weight: bold"></div>').html(' (zasebno za '+spor.prej+ '): ' +sporocilo);
        $('#sporocila').append(novElement);
      }
    } else {
      $('#sporocila').append(divElementHtmlTekst(spor.sporocilo));
    }
  });
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      mojVzd = rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      vzd = rezultat.vzdevek;
      $('#kanal').text(rezultat.vzdevek + ' @ ' + kan);
    } else {
      sporocilo = censor(sporocilo);
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    kan = rezultat.kanal;
    $('#kanal').text(vzd + ' @ ' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('kanaliGeslo', function (sporocilo) {
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('uporabniki', function (uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var u in uporabniki) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[u]));
    }
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});